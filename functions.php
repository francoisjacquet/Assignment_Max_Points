<?php
/**
 * Functions
 *
 * @package Assignment Max Points
 */

add_action( 'Warehouse.php|footer', 'AssignmentMaxPointsDo' );

/**
 * Do Assignment Max Points action
 *
 * Inject JS to the footer: global vars + plugins/Assignment_Max_Points/js/scripts.js file
 *
 * Only if we are on Assignments / Mass Create Assignments program,
 * or we are on Gradebook Grades (Teacher Programs) program.
 *
 * @return bool True if JS injected
 */
function AssignmentMaxPointsDo()
{
	// Check we are on Assignments / Mass Create Assignments program.
	// or check we are on Gradebook Grades (Teacher Programs) program.
	if ( $_REQUEST['modname'] === 'Grades/MassCreateAssignments.php'
		|| $_REQUEST['modname'] === 'Grades/Assignments.php'
		|| $_REQUEST['modname'] === 'Grades/Grades.php'
		|| $_REQUEST['modname'] === 'Users/TeacherPrograms.php&include=Grades/Grades.php' )
	{
		$max_points = Config( 'ASSIGNMENT_MAX_POINTS' );

		$use_scale_value = Config( 'ASSIGNMENT_MAX_POINTS_USE_SCALE_VALUE' );

		if ( $use_scale_value
			&& UserCoursePeriod() )
		{
			$scale_value = DBGetOne( "SELECT GP_SCALE
				FROM report_card_grade_scales
				WHERE SCHOOL_ID='" . UserSchool() . "'
				AND SYEAR='" . UserSyear() . "'
				AND ID=(SELECT GRADE_SCALE_ID
					FROM course_periods
					WHERE COURSE_PERIOD_ID='" . UserCoursePeriod() . "')" );

			if ( $scale_value )
			{
				$max_points = $scale_value;
			}
		}

		if ( ! $max_points )
		{
			return false;
		}

		// Inject JS to set Max Assignment Points.
		// or inject JS to prevent saving Grades > Max Points.
		// Hidden input values are used by scripts.js
		?>
		<input type="hidden" disabled id="assignment_max_points" value="<?php echo AttrEscape( (int) $max_points ); ?>" />
		<input type="hidden" disabled id="assignment_max_points_gradebook_error" value="<?php echo AttrEscape( sprintf( dgettext(
				'Assignment_Max_Points',
				'Some points are over the maximum of %d, please correct.'
			), $max_points ) ); ?>" />
		<script src="plugins/Assignment_Max_Points/js/scripts.js?v=1.2"></script>
		<?php

		return true;
	}

	return false;
}
