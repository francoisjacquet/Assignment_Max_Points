��          L      |       �      �      �   V   �   7   /     g         �     �  k   �  B   $  ,   g                                         Assignment Max Points Assignment Max Points %s If a course is graded, use the value of its grading scale instead of the above points. Some points are over the maximum of %d, please correct. Use Grading Scale Value Project-Id-Version: Assignment Max Points plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-12-11 17:59+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Tareas puntos máx. Tareas puntos máx. %s Si un curso está calificado, usar el valor de la escala de calificaciones en vez de los puntos anteriores. Algunos puntos están encima del máximo de %d, corrija por favor. Usar el valor de la escala de calificaciones 