��          L      |       �      �      �   V   �   7   /     g         �     �  c   �  >     ,   Z                                         Assignment Max Points Assignment Max Points %s If a course is graded, use the value of its grading scale instead of the above points. Some points are over the maximum of %d, please correct. Use Grading Scale Value Project-Id-Version: Assignment Max Points plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-12-11 18:01+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Devoirs points max. Devoirs points max. %s Si un cours est noté, utiliser la valeur de son échelle de notation au lieu des points ci-dessus. Des points sont au dessus du maximum de %d, merci de corriger. Utiliser la valeur de l'échelle de notation 