# Assignment Max Points plugin

![screenshot](https://gitlab.com/francoisjacquet/Assignment_Max_Points/raw/master/screenshot.png?inline=false)

https://gitlab.com/francoisjacquet/Assignment_Max_Points/

Version 1.3 - December, 2024

Author François Jacquet

Sponsored by Ecole Étoile du Matin, France

License GNU/GPLv2 or later

## Description

This plugin lets you set a maximum of Points for Assignments.

1. When (mass) creating or editing an Assignment, total "Points" will be forced to the maximum, no more, no less.
2. When entering Gradebook Grades, "Points" over the maximum will prevent from saving the form and an alert will be displayed to teachers.

The maximum points value apply for all teachers (and administrators), and different values can be set for each school.

Note: Instead of using a fixed maximum of points for all courses, you can choose to use the grading scale value associated to the course (if the course is graded).

Translated in French & Spanish.

## Content

Plugin Configuration

- Assignment Max Points
- Use Grading Scale Value

## Install

Copy the `Assignment_Max_Points/` folder (if named `Assignment_Max_Points-master`, rename it) and its content inside the `plugins/` folder of RosarioSIS.

Go to _School > Configuration > Plugins_ and click "Activate".

Requires RosarioSIS 6.8+
